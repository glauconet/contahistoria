package com.contahistoria;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MasterFragment extends ListFragment {

    Activity   activity;
    public DetailFragment detailFragment;

    public static String tipo;

    private static final String DEBUG= "debugar";


    public List<String> lista = new ArrayList<String>();


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Log.i(DEBUG, "Em MasterFragment    " + tipo) ;
        if(tipo.equals("history")) {

            lista.addAll(Arrays.asList(getResources().getStringArray(R.array.hystorys)));

        }

        if(tipo.equals("skills")) {

            lista.addAll(Arrays.asList(getResources().getStringArray(R.array.skills)));
        }


        if(tipo.equals("bussines")) {

           // lista.addAll(Arrays.asList(getResources().getStringArray(R.array.skills)));
        }
        setListAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, lista));
        carregar(this.getView());// não teve o evento ainda, por isso chama esse

    }



    public void onListItemClick(ListView parent, View v, int position, long id) {


        String   selectedItens = lista.get(position);

        detailFragment = (DetailFragment) getFragmentManager().findFragmentById(R.id.detailFragment);
        
        //---if the detail fragment is not in the current activity as myself---
        if(!tipo.equals("bussines")) {

            if (detailFragment != null && detailFragment.isInLayout()){

                //---the detail fragment is in the same activity as the master---
                detailFragment.setSelectedItem(selectedItens, position, tipo);

                //Toast.makeText(getActivity(), "Selecionado-" + "position:" + position +  "selectedItem:"+ selectedItens + " tipo:"+tipo, Toast.LENGTH_SHORT).show();

            }else{

                //---the detail fragment is in its own activity---
                Intent intent = new Intent(getActivity(), DetailActivity.class);

                intent.putExtra("item", selectedItens);
                intent.putExtra("posicao",position);
                intent.putExtra("tipo",tipo);

                startActivity(intent);

            }

        }else{ // igual a bussines

            if (detailFragment != null && detailFragment.isInLayout()){

                //---the detail fragment is in the same activity as the master---
                detailFragment.setSelectedItem(selectedItens, position, tipo);

                //Toast.makeText(getActivity(), "Selecionado-" + "position:" + position +  "selectedItem:"+ selectedItens + " tipo:"+tipo, Toast.LENGTH_SHORT).show();

            } else {

                //---the detail fragment is in its own activity---
                Intent intent = new Intent(getActivity(), DetailActivity.class);

                intent.putExtra("item", selectedItens);
                intent.putExtra("posicao",position);
                intent.putExtra("tipo",tipo);

                startActivity(intent);

            }

        }


    }

    public void carregar(View v) {


        String selectedPresident = "Selecione um Item";
        int i = 0;

        detailFragment = (DetailFragment) getFragmentManager().findFragmentById(R.id.detailFragment);

        //---if the detail fragment is not in the current activity as myself---
        if (detailFragment != null && detailFragment.isInLayout()){

            //---the detail fragment is in the same activity as the master---
            detailFragment.setSelectedItem(selectedPresident, i, null);

            //Toast.makeText(getActivity(), "You have selected " + i, Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(activity instanceof HomeScreen) {

            this.activity = activity;

            this.tipo =  ((HomeScreen) activity).parametro ;


            if(tipo.equals("bussines")) {

                lista.addAll(((HomeScreen) activity).listaActivity);
            }

        }

    }
}

