package com.contahistoria;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DetailFragment extends Fragment {

    Activity   activity;
    public static String tipo;

    public List<String> lista = new ArrayList<String>();
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.activity_detail, container, false);

        //TextView textView = (TextView) view.findViewById(R.id.txtSelectedPresident);
       // textView.setText("Escolha Uma História!");
        WebView webView = (WebView) view.findViewById(R.id.WebView01);

        webView.loadUrl("file:///android_asset/inicial.html");

        return view;
    }

    public void setSelectedItem(String name, int posicao, String tipo) {


        if(!tipo.equals("bussines")) {
            WebView webView = (WebView) getView().findViewById(R.id.WebView01);

            webView.loadUrl("file:///android_asset/"+tipo+"/index"+posicao+".html");
        }else{


            TextView view = (TextView) getView().findViewById(R.id.txtSelected);
            view.setText("You have selected");
            view.setText("You have selected " + lista.get(posicao));

        }


    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


        if(activity instanceof HomeScreen) {

            this.activity = activity;

            this.tipo =  ((HomeScreen) activity).parametro ;


            if(tipo.equals("bussines")) {

                lista.addAll(((HomeScreen) activity).listaTexto);
            }

        }

    }


}

