package com.contahistoria;

import android.app.Activity;
import android.content.CursorLoader;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends Activity {

    /** Called when the activity is first created. */
    private static final String DEBUG= "debugar";
    public List<String> listaTexto = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //---if the user switches to landscape mode; destroy the activity---
        if (getResources().getConfiguration().orientation ==   Configuration.ORIENTATION_LANDSCAPE) {
            finish();
            return;
        }

        //---get the data passed from the master fragment---
        String name = getIntent().getStringExtra("item");
        int posicao = getIntent().getIntExtra("posicao",0);
        String tipo = getIntent().getStringExtra("tipo");

        //Toast.makeText(this, "You have selected " + posicao, Toast.LENGTH_SHORT).show();
        // TextView view = (TextView) findViewById(R.id.txtSelectedPresident);

        //view.setText("You have selected " + name);
        if(!tipo.equals("bussines")) {
            WebView webView = (WebView) findViewById(R.id.WebView01);
            // webView.loadUrl("file:///android_asset/index"+posicao+".html");
            webView.loadUrl("file:///android_asset/"+tipo+"/index"+posicao+".html");

        }else{

            Uri allTitles = Uri.parse("content://net.learn2develop.provider.Books/books");


            Cursor c;
            if(android.os.Build.VERSION.SDK_INT <11) {
                //---before Honeycomb---
                c = managedQuery(allTitles, null, null, null, "title desc");
            }else{
                //---Honeycomb and later---
                CursorLoader cursorLoader = new CursorLoader(this,	allTitles, null, null, null,"title desc");
                c = cursorLoader.loadInBackground();
            }

            if(c.moveToFirst()){

                do{

                    listaTexto.add(c.getString(c.getColumnIndex(BooksProvider.TITLE)));
                    // c.getString(c.getColumnIndex(BooksProvider._ID))
                    // c.getString(c.getColumnIndex(BooksProvider.TITLE))
                    // c.getString(c.getColumnIndex(BooksProvider.ISBN))

                } while (c.moveToNext());
            }
            TextView view = (TextView) findViewById(R.id.txtSelected);
            view.setText("You have selected");
            view.setText("You have selected " + listaTexto.get(posicao));

        }


    }
}
