package com.contahistoria;

import android.content.CursorLoader;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;

import java.util.ArrayList;
import java.util.List;

public class HomeScreen extends FragmentActivity{

    String parametro;

    private static final String DEBUG= "debugar";
    public List<String> listaActivity = new ArrayList<String>();
    public List<String> listaTexto = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        Intent it  = getIntent();
        if (it!= null) {
             parametro = it.getStringExtra("acao");

        }


         if( (parametro.equals("history"))  || (parametro.equals("skills"))){

             Log.i(DEBUG, "Escolheu   " + parametro) ;

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                //---landscape mode---
                setContentView(R.layout.activity_landscape_main);

            } else {
                //---portrait mode---
                setContentView(R.layout.activity_main);
            }

         }



        if(parametro.equals("dictionary")){

                //---portrait mode---
            Intent intent = new Intent(this, DetailActivity.class);

           // intent.putExtra("item", selectedItens);
            //intent.putExtra("posicao",position);
            //intent.putExtra("tipo",tipo);

            startActivity(intent);


        }





        if(parametro.equals("bussines")){

           // listaActivity.addAll(Arrays.asList(getResources().getStringArray(R.array.skills)));
            Uri allTitles = Uri.parse("content://net.learn2develop.provider.Books/books");

            Log.i(DEBUG, "Escolheu   " + parametro) ;
            Cursor c;
            if(android.os.Build.VERSION.SDK_INT <11) {
                //---before Honeycomb---
                c = managedQuery(allTitles, null, null, null, "title desc");
            }else{
                //---Honeycomb and later---
                CursorLoader cursorLoader = new CursorLoader(this,	allTitles, null, null, null,"title desc");
                c = cursorLoader.loadInBackground();
            }

            if(c.moveToFirst()){

                do{
                    listaActivity.add(c.getString(c.getColumnIndex(BooksProvider.ISBN)));
                    listaTexto.add(c.getString(c.getColumnIndex(BooksProvider.TITLE)));
                    //Toast.makeText(this, c.getString(c.getColumnIndex(BooksProvider._ID)) + ", " +
                       //     c.getString(c.getColumnIndex(BooksProvider.TITLE)) + ", " +
                        //    c.getString(c.getColumnIndex(BooksProvider.ISBN)), Toast.LENGTH_SHORT).show();

                } while (c.moveToNext());
            }



            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                //---landscape mode---
                setContentView(R.layout.activity_landscape_main);

            } else {
                //---portrait mode---
                setContentView(R.layout.activity_main);
            }



        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }


}
