package com.contahistoria;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class Main extends Activity implements OnClickListener {
	private Button bt1;
	private Button bt2;
	private Button bt3;
	private Button bt4;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Layout do Dashboard
		setContentView(R.layout.main);
		bt1 = (Button) findViewById(R.id.bt1);
		bt1.setOnClickListener(this);

		bt2 = (Button) findViewById(R.id.bt2);
		bt2.setOnClickListener(this);

		bt3 = (Button) findViewById(R.id.bt3);
		bt3.setOnClickListener(this);

		bt4 = (Button) findViewById(R.id.bt4);
		bt4.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {

        Intent intent = new Intent(this, HomeScreen.class);

        switch(v.getId()){

            case R.id.bt1 :
                intent.putExtra("acao", "history");
            break;

            case R.id.bt2 :
                intent.putExtra("acao","skills");
            break;

            case R.id.bt3 :
                intent.putExtra("acao", "dictionary");
            break;

            case R.id.bt4 :
                intent.putExtra("acao", "bussines");
            break;

        }


        if(intent != null) {
            startActivity(intent);
        }


	}
}
